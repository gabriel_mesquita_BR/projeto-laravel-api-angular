<?php

namespace App\Http\Controllers;

use App\Covid;

class CovidController extends Controller
{

    public function info()
    {
        try {

            $covid     = new Covid();

            $content   = $covid->getData();

            $date_time = new \DateTime($content['lastUpdatedAtApify']);

            $date_time = $date_time->setTimeZone(new \DateTimeZone('America/Fortaleza'));

            $date_time = $date_time->format('d-m-Y') . ' ' . $date_time->format('H:m');

            $content   = [$date_time, $content['recovered'], $content['infected'],
                $content['deceased']];

            return response()->json(compact('content'));

        }catch(\Exception $exception) {

            throw new \Exception('Error in the Request Made');
        }

    }

    public function infectedByRegion()
    {
        try {

            $covid     = new Covid();

            $content   = $covid->getData();

            $content   = $content['infectedByRegion'];

            return response()->json(compact('content'));

        }catch(\Exception $exception) {

            throw new \Exception('Error in the Request Made');
        }
    }
}
