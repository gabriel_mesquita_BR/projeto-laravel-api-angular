<?php

namespace App;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class Covid extends Model
{
    public function getData()
    {
        $client   = new Client();

        $response = $client->request('GET',
            'https://api.apify.com/v2/key-value-stores/TyToNta7jGKkpszMZ/records/LATEST?disableRedirect=true');

        $content  = $response->getBody()->getContents();

        $content  = json_decode($content, true);

        return $content;
    }
}
