## VERSÕES

### PHP 7.2
### Laravel 5.8
### Angular 7

## PASSOS PARA A EXECUÇÃO DO PROJETO

Laravel Api com Angular

composer global require laravel/installer => instala globalmente o instalador do laravel

git clone https://gabriel_mesquita_BR@bitbucket.org/gabriel_mesquita_BR/projeto-laravel-api-angular.git

Dentro de Laravel-Api: 

composer install => instala as dependências
php artisan serve => executa a aplicação via php built-in server

Dentro de Angular-Consumindo-Api-Framework: 

npm install => instala as dependências
ng serve => executar a aplicação angular